# FIdO

Fido is a Discord bot written to interact on Discord with the FIO data available from the fnar/fio/fiorest> server.

## Using

Make sure to copy `config.toml.sample` to `config.toml` and edit your settings (especially the discord token) before launching.

### API token for FIO

It's sort of preferable for the bot to use a static token instead of having to login and then refresh the token every 24 hours.  Use https://doc.fnar.net/#/auth/post_auth_createapikey to create a token and then put that into the config file as well.
