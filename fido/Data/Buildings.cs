﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Fido.Web;
using FIOWeb.JsonPayloads;
using Microsoft.Extensions.Caching.Memory;
using Serilog;

namespace Fido.Data
{
    public class Buildings
    {
        public static TimeSpan cacheExpireTime = TimeSpan.FromDays(30);
        public static MemoryCache buildingCache = new MemoryCache(new MemoryCacheOptions());

        static Buildings()
        {
            
        }

        private static async Task<List<BuildingPayload>> GetBuildingList()
        {
            List<BuildingPayload> list = buildingCache.Get<List<BuildingPayload>>("buildinglist");
            if (list == null)
            {
                Log.Information("Reaching out to server for Building Data");
                Web.Request request= new Web.Request(HttpMethod.Get, "/building/allbuildings");
                list = await request.GetResponseAsync<List<BuildingPayload>>();
                buildingCache.Set<List<BuildingPayload>>("buildinglist", list, cacheExpireTime);
            }
            return list;
        }

        public static async Task<List<BuildingPayload>> FindByName(String input)
        {
            List<BuildingPayload> buildingList = await GetBuildingList();
            if (buildingList == null)
            {
                Log.Error("Failed to get a building list from `/building/allbuildings`.\n");
                return null;
            }
            List<BuildingPayload> buildings = buildingList.FindAll(x => x.Name.ToUpper().Contains(input.ToUpper()));
            return buildings;
        }

        public static async Task<BuildingPayload> FindByTicker(String ticker)
        {
            List<BuildingPayload> buildingList = await GetBuildingList();
            if (buildingList == null)
            {
                Log.Error("Failed to get a building list from `/building/allbuildings`.\n");
                return null;
            }
            BuildingPayload building = buildingList.Find(x => x.Ticker.ToUpper().Equals(ticker.ToUpper()));
            return building;
        }
    }
}
