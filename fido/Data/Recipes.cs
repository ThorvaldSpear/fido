﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Fido.Web;
using FIOWeb.JsonPayloads;
using Microsoft.Extensions.Caching.Memory;
using Serilog;
using System.Linq;

namespace Fido.Data
{
    public class Recipes
    {
        public static TimeSpan cacheExpireTime = TimeSpan.FromDays(30);
        public static MemoryCache cache = new MemoryCache(new MemoryCacheOptions());

        static Recipes()
        {

        }

        private static async Task<List<RecipePayload>> GetRecipeList()
        {
            List<RecipePayload> list = cache.Get<List<RecipePayload>>("recipes");
            if (list == null)
            {
                Log.Information("Reaching out to server for Recipe Data");
                Web.Request request = new Web.Request(HttpMethod.Get, "/recipes/allrecipes");
                list = await request.GetResponseAsync<List<RecipePayload>>();
                cache.Set<List<RecipePayload>>("recipes", list, cacheExpireTime);
            }
            return list;
        }

        public static async Task<List<RecipePayload>> FindByInputTicker(String ticker)
        {
            List<RecipePayload> list = await GetRecipeList();
            if (list == null)
            {
                Log.Error("Failed to get a material list from `/recipes/allrecipes`.\n");
                return null;
            }
            List<RecipePayload> entries = list.FindAll(rec => rec.Inputs.FindAll(io => io.Ticker.ToUpper().Equals(ticker.ToUpper())).Count > 0);
            return entries;
        }

        public static async Task<List<RecipePayload>> FindByOutputTicker(String ticker)
        {
            List<RecipePayload> list = await GetRecipeList();
            if (list == null)
            {
                Log.Error("Failed to get a material list from `/recipes/allrecipes`.\n");
                return null;
            }
            List<RecipePayload> entries = list.FindAll(rec => rec.Outputs.FindAll(io => io.Ticker.ToUpper().Equals(ticker.ToUpper())).Count > 0);
            return entries;
        }

        public static string TimeString(int ms_input)
        {
            int days = ms_input / 86400000; // ms in 1 day
            int hours = (ms_input % 86400000) / 3600000; // ms in an hour
            int minutes = (ms_input % 3600000) / 60000; // ms in a minute
            int seconds = (ms_input % 60000) / 1000;
            if (days > 0)
                return $"{days}d {hours}h";
            if (hours > 0)
                return $"{hours}h {minutes}m";
            if (minutes > 0)
                return $"{minutes}m {seconds}s";
            return $"{seconds}s";
        }

    }
}
