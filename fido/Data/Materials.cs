﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Fido.Web;
using FIOWeb.JsonPayloads;
using Microsoft.Extensions.Caching.Memory;
using Serilog;

namespace Fido.Data
{
    public class MaterialColor
    {
        public byte r, g, b;
        public MaterialColor()
        {
            r = 0; g = 0; b = 0;
        }
        public MaterialColor(byte in_r, byte in_g, byte in_b)
        {
            r = in_r; g = in_g; b = in_b;
        }
    }

    public class Materials
    {
        public static TimeSpan cacheExpireTime = TimeSpan.FromDays(30);
        public static MemoryCache cache = new MemoryCache(new MemoryCacheOptions());

        static Materials()
        {

        }

        private static async Task<List<MaterialPayload>> GetMaterialList()
        {
            List<MaterialPayload> list = cache.Get<List<MaterialPayload>>("materiallist");
            if (list == null)
            {
                Log.Information("Reaching out to server for Material Data");
                Web.Request request = new Web.Request(HttpMethod.Get, "/material/allmaterials");
                list = await request.GetResponseAsync<List<MaterialPayload>>();
                cache.Set<List<MaterialPayload>>("materiallist", list, cacheExpireTime);
            }
            return list;
        }

        public static async Task<MaterialPayload> FindById(String input)
        {
            List<MaterialPayload> list = await GetMaterialList();
            if (list == null)
            {
                Log.Error("Failed to get a material list from `/materials/allmaterials`.\n");
                return null;
            }
            MaterialPayload material = list.Find(x => x.MatId.ToUpper().Equals(input.ToUpper()));
            return material;
        }

        public static async Task<List<MaterialPayload>> FindByName(String input)
        {
            List<MaterialPayload> list = await GetMaterialList();
            if (list == null)
            {
                Log.Error("Failed to get a material list from `/material/allmaterials`.\n");
                return null;
            }
            List<MaterialPayload> entries = list.FindAll(x => x.Name.ToUpper().Contains(input.ToUpper()));
            return entries;
        }

        public static async Task<MaterialPayload> FindByTicker(String ticker)
        {
            List<MaterialPayload> list = await GetMaterialList();
            if (list == null)
            {
                Log.Error("Failed to get a material list from `/materials/allmaterials`.\n");
                return null;
            }
            MaterialPayload material = list.Find(x => x.Ticker.ToUpper().Equals(ticker.ToUpper()));
            return material;
        }

        public static MaterialColor GetMaterialColor(MaterialPayload material)
        {
            if (material.CategoryName.Equals("agricultural products"))
                return new MaterialColor(92, 18, 18);
            if (material.CategoryName.Equals("alloys"))
                return new MaterialColor(77, 77, 77);
            if (material.CategoryName.Equals("chemicals"))
                return new MaterialColor(183, 46, 91);
            if (material.CategoryName.Equals("construction materials"))
                return new MaterialColor(24, 91, 211);
            if (material.CategoryName.Equals("construction parts"))
                return new MaterialColor(35, 30, 68);
            if (material.CategoryName.Equals("construction prefabs"))
                return new MaterialColor(54, 54, 54);
            if (material.CategoryName.Equals("consumables (basic)"))
                return new MaterialColor(73, 85, 96);
            if (material.CategoryName.Equals("consumables (luxury)"))
                return new MaterialColor(9, 15, 15);
            if (material.CategoryName.Equals("drones"))
                return new MaterialColor(91, 46, 183);
            if (material.CategoryName.Equals("electronic devices"))
                return new MaterialColor(86, 20, 147);
            if (material.CategoryName.Equals("electronic parts"))
                return new MaterialColor(92, 30, 122);
            if (material.CategoryName.Equals("electronic pieces"))
                return new MaterialColor(73, 85, 97);
            if (material.CategoryName.Equals("electronic systems"))
                return new MaterialColor(49, 24, 7);
            if (material.CategoryName.Equals("elements"))
                return new MaterialColor(116, 71, 208);
            if (material.CategoryName.Equals("energy systems"))
                return new MaterialColor(51, 24, 216);
            if (material.CategoryName.Equals("fuels"))
                return new MaterialColor(30, 123, 30);
            if (material.CategoryName.Equals("gases"))
                return new MaterialColor(67, 77, 87);
            if (material.CategoryName.Equals("liquids"))
                return new MaterialColor(80, 41, 23);
            if (material.CategoryName.Equals("medical equipment"))
                return new MaterialColor(9, 15, 15);
            if (material.CategoryName.Equals("metals"))
                return new MaterialColor(16, 92, 87);
            if (material.CategoryName.Equals("minerals"))
                return new MaterialColor(73, 85, 97);
            if (material.CategoryName.Equals("ores"))
                return new MaterialColor(57, 95, 96);
            if (material.CategoryName.Equals("plastics"))
                return new MaterialColor(26, 60, 162);
            if (material.CategoryName.Equals("ship engines"))
                return new MaterialColor(14, 57, 14);
            if (material.CategoryName.Equals("ship kits"))
                return new MaterialColor(29, 36, 16); // (54, 61, 41)
            if (material.CategoryName.Equals("ship parts"))
                return new MaterialColor(59, 45, 184); // (84, 70, 173)
            if (material.CategoryName.Equals("ship shields"))
                return new MaterialColor(132, 82, 34); // (157, 107, 59)
            if (material.CategoryName.Equals("software components"))
                return new MaterialColor(67, 77, 87); // (92, 102, 112)
            if (material.CategoryName.Equals("software systems"))
                return new MaterialColor(26, 60, 162); // (51, 85, 187)
            if (material.CategoryName.Equals("software tools"))
                return new MaterialColor(6, 6, 29); // (31, 31, 54)
            if (material.CategoryName.Equals("textiles"))
                return new MaterialColor(80, 41, 23); // (105, 66, 48)
            if (material.CategoryName.Equals("unit prefabs"))
                return new MaterialColor(59, 45, 148); // (84, 70, 173)
            if (material.CategoryName.Equals("utility"))
                return new MaterialColor(54, 54, 54); // (79, 79, 79)
            return new MaterialColor(255, 255, 255);
        }
    }
}
