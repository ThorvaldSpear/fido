﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Fido.Web;
using FIOWeb.JsonPayloads;
using Microsoft.Extensions.Caching.Memory;
using Serilog;

namespace Fido.Data
{
    public class Exchanges
    {
        public static TimeSpan cacheExpireTime = TimeSpan.FromDays(30);
        public static TimeSpan cacheOrderExpireTime = TimeSpan.FromHours(1);
        public static MemoryCache cache = new MemoryCache(new MemoryCacheOptions());

        public Exchanges()
        {
        }

        public static async Task<List<ExchangePayload>> GetExchangeList()
        {
            List<ExchangePayload> list = cache.Get<List<ExchangePayload>>("exchangelist");
            if (list == null)
            {
                Log.Information("Reaching out to server for Exchange List");
                Web.Request request = new Web.Request(HttpMethod.Get, "/global/comexexchanges");
                list = await request.GetResponseAsync<List<ExchangePayload>>();
                list.Sort((a, b) => a.ExchangeCode.CompareTo(b.ExchangeCode));
                cache.Set<List<ExchangePayload>>("exchangelist", list, cacheExpireTime);
            }
            return list;
        }

        public static async Task<ExchangeOrderPayload> GetExchangeOrder(string ticker)
        {
            ExchangeOrderPayload entry = cache.Get<ExchangeOrderPayload>($"cx-{ticker}");
            if (entry == null)
            {
                Log.Information("Reaching out to server for Exchange Order");
                Web.Request request = new Web.Request(HttpMethod.Get, $"/exchange/{ticker}");
                entry = await request.GetResponseAsync<ExchangeOrderPayload>();
                cache.Set<ExchangeOrderPayload>($"cx-{ticker}", entry, cacheOrderExpireTime);
            }
            return entry;
        }

        public static async Task<ExchangePayload> FindByCode(String input)
        {
            List<ExchangePayload> list = await GetExchangeList();
            if (list == null)
            {
                Log.Error("Failed to get an exchange list from /global/exchanges`.\n");
                return null;
            }
            ExchangePayload entry = list.Find(x => x.ExchangeCode.ToUpper().Equals(input.ToUpper()));
            return entry;
        }

        public static async Task<List<ExchangePayload>> FindByName(String input)
        {
            List<ExchangePayload> list = await GetExchangeList();
            if (list == null)
            {
                Log.Error("Failed to get an exchange list from /global/exchanges`.\n");
                return null;
            }
            List<ExchangePayload> entries = list.FindAll(x => x.ExchangeName.ToUpper().Contains(input.ToUpper()));
            return entries;
        }

    }
}
