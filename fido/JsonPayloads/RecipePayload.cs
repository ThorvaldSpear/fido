﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FIOWeb.JsonPayloads
{
    public class RecipeInput
    {
        public string Ticker { get; set; }
        public int Amount { get; set; }
    }

    public class RecipeOutput
    {
        public string Ticker { get; set; }
        public int Amount { get; set; }
    }

    public class RecipePayload
    {
        public string BuildingTicker { get; set; }
        public string RecipeName { get; set; }
        public List<RecipeInput> Inputs { get; set; } = new List<RecipeInput>();
        public List<RecipeOutput> Outputs { get; set; } = new List<RecipeOutput>();
        public int TimeMs { get; set; }
    }

}