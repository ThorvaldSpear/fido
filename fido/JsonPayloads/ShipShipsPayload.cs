using System;

namespace FIOWeb.JsonPayloads
{
    public class ShipShipsPayload
    {
        public Ship[] Ships { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Ship
    {
        public RepairMaterial[] RepairMaterials { get; set; }
        public string ShipId { get; set; }
        public string StoreId { get; set; }
        public string StlFuelStoreId { get; set; }
        public string FtlFuelStoreId { get; set; }
        public string Registration { get; set; }
        public string Name { get; set; }
        public long CommissioningTimeEpochMs { get; set; }
        public object BlueprintNaturalId { get; set; }
        public string FlightId { get; set; }
        public float Acceleration { get; set; }
        public float Thrust { get; set; }
        public float Mass { get; set; }
        public float OperatingEmptyMass { get; set; }
        public float ReactorPower { get; set; }
        public float EmitterPower { get; set; }
        public float Volume { get; set; }
        public float Condition { get; set; }
        public object LastRepairEpochMs { get; set; }
        public string Location { get; set; }
        public float StlFuelFlowRate { get; set; }
    }

    public class RepairMaterial
    {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public int Amount { get; set; }
    }
}
