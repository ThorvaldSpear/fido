﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System.Threading.Tasks;
using System;
using Serilog;
using Tommy;
using System.IO;
using DSharpPlus.Entities;
using System.Collections.Generic;

namespace Fido.commands
{
    public class ChatCommands : BaseCommandModule
    {
        public ChatCommands()
        {
        }


        public static void ReloadBlame()
        {
            try
            {
                using (StreamReader reader = new StreamReader(File.OpenRead("blame.toml")))
                {
                    Globals.blame = TOML.Parse(reader);
                    Log.Debug("Loaded blame.toml file");
                }
            }
            catch
            {
                Log.Error("Problem reading blame.toml file.  Exiting.\n");
                System.Environment.Exit(1);
            }
        }

        [Command("blame")]
        [DescriptionAttribute("Blame FIOC for something")]
        public async Task BlameCommand(CommandContext ctx)
        {
            await BlameCommand(ctx, "FIOC");
        }

        [Command("blame")]
        [DescriptionAttribute("Blame a group for something (must be in the blame file)")]
        public async Task BlameCommand(CommandContext ctx, string group)
        {
            if (Globals.blame != null && group == "*")
            {
                var rand = new Random();
                int count = 0;
                foreach(var key in Globals.blame.Keys)
                {
                    count += Globals.blame[key].ChildrenCount;
                }
                int randidx = rand.Next(count);
                foreach(var key in Globals.blame.Keys)
                {
                    if(randidx < Globals.blame[key].ChildrenCount)
                    {
                        group = key;
                        break;
                    }
                    randidx -= Globals.blame[key].ChildrenCount;
                }
                Log.Debug($"selected random group: {group}");
            }
            var response = new DiscordEmbedBuilder();
            if (Globals.blame != null && Globals.blame.HasKey(group.ToLower()))
            {
                var rand = new Random();
                var responseOptions = Globals.blame[group.ToLower()];
                response.WithColor(new DiscordColor(79, 138, 16));
                response.AddField($"Blame - {group.ToUpper()}", responseOptions[rand.Next(0, responseOptions.ChildrenCount)]);
                await ctx.RespondAsync(response);
            }
            else
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.AddField($"BlameFIOC", $"FIOC broke my blame list!  I'm not sure how to blame {group}");
                await ctx.RespondAsync(response);
            }
        }

        [Command("reloadblame")]
        [DescriptionAttribute("Reload the blame file.")]
        [Hidden]
        [RequireOwner]
        public async Task ReloadBlameCommand(CommandContext ctx)
        {
            ReloadBlame();
            await ctx.RespondAsync("Reloaded.");
        }
    }
}
