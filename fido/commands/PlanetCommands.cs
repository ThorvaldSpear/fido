﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System.Threading.Tasks;
using System;
using FIOWeb.JsonPayloads;
using Fido.Data;
using Serilog;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSharpPlus.Entities;

namespace Fido.commands
{
    public class PlanetCommands : BaseCommandModule
    {
        public PlanetCommands()
        {
        }

        [Command("pli")]
        [Aliases("planetinfo")]
        [DescriptionAttribute("Get information on a specific planet")]
        public async Task PliCommand(CommandContext ctx,
            [DescriptionAttribute("Name or Natural ID of the planetto lookup")]
            string planet)
        {
            await PliCommand(ctx, planet, 100);
        }

        [Command("pli")]
        [DescriptionAttribute("Get information on a specific planet")]
        public async Task PliCommand(CommandContext ctx,
            [DescriptionAttribute("Name or Natural ID of the planetto lookup")]
            string planet,
            [DescriptionAttribute("Resource Extraction Percentage (eg: \"100\" for 100%)")]
            double respercent)
        {
            PlanetDataPayload pdp = await Planets.Find(planet);
            var response = new DiscordEmbedBuilder();

            if (pdp == null)
            {
                List<PlanetDataPayload> pdplist = await Planets.FindPartial(planet);
                if (pdplist.Count > 1)
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: multiple found", "Multiple planets were found.  Please be more specific.");
                    string lresponse = "```\n";
                    foreach (PlanetDataPayload entry in pdplist)
                    {
                        lresponse += $"{entry.PlanetNaturalId,-4} : {entry.PlanetName}\n";
                    }
                    lresponse += "```";
                    response.AddField("Matches", lresponse);
                    await ctx.RespondAsync(response);
                    return;
                }
                if (pdplist.Count == 0)
                {
                    response.WithColor(new DiscordColor(216, 0, 12));
                    response.AddField("Error: no match", "No matching planets were found.");
                    await ctx.RespondAsync(response);
                    return;
                }
                pdp = pdplist[0];
            }
            response.WithColor(new DiscordColor(79, 138, 16));

            Log.Debug($"found planet: {pdp.PlanetNaturalId} {pdp.PlanetName}");
            response.Title = $"{pdp.PlanetNaturalId} : {pdp.PlanetName}";
            // misc fields
            string plenv = "```\n";
            plenv += $"Gravity:  {Planets.EnvHighLow(pdp.Gravity, 0.25, 2.5)}\n";
            plenv += $"Temp.:    {Planets.EnvHighLow(pdp.Temperature, -25.0, 75.0)}\n";
            plenv += $"Pressure: {Planets.EnvHighLow(pdp.Pressure, 0.25, 2.0)}\n";
            plenv += $"Fertile:  {Planets.FertilityStr(pdp)}\n";
            plenv += $"Surface:  {(pdp.Surface ? "Rocky" : "Gaseous")}\n";
            plenv += "```";
            response.AddField("Environment", plenv, true);
            string plprojects = "```\n";
            if (pdp.HasAdministrationCenter)
                plprojects += "ADM\n";
            if (pdp.HasChamberOfCommerce)
                plprojects += "CoGC\n";
            if (pdp.HasLocalMarket)
                plprojects += $"LM   ({pdp.BaseLocalMarketFee * pdp.LocalMarketFeeFactor})\n";
            if (pdp.HasShipyard)
                plprojects += "SHY\n";
            if (pdp.HasWarehouse)
                plprojects += $"WAR  ({pdp.WarehouseFee})\n";
            plprojects += "```";
            response.AddField("Infrastructure", plprojects, true);
            // Resources
            if(pdp.Resources.Count > 0)
            {
                string datalist = "```\n";
                foreach (Resource res in pdp.Resources)
                {
                    MaterialPayload mat = await Materials.FindById(res.MaterialId);
                    datalist += $"{res.Factor * Planets.ResFactor(pdp) * (respercent/100.0):N2} > {mat.Ticker}\n";
                }
                datalist += "```";
                response.AddField($"Resources (Daily @ {respercent}%)", datalist, true);
            }
            if (pdp.CurrencyCode != null)
                response.AddField("Currency", $"{pdp.CurrencyCode}", true);
            if (pdp.GovernorId != null)
                response.AddField("Governer", pdp.GovernorUserName, true);
            if (pdp.GovernorCorporationId != null)
                response.AddField("Gov.Corp.", $"({pdp.GovernorCorporationCode}) {pdp.GovernorCorporationName}", true);
            if (pdp.FactionCode != null)
                response.AddField("Faction", $"({pdp.FactionCode}) {pdp.FactionName}", true);

            await ctx.RespondAsync(response);
        }

    }
}
