﻿using System;
using System.IO;
using System.Threading.Tasks;
using Fido.Cache;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Tommy;
using Fido.Data;

namespace Fido
{
    public static class Globals
    {
        /* Yeah, sure globals are bad, and whatnot, but I want configuration
         * to be easy & straightforward. */
        public static TomlTable config { get; set; }
        public static WaitToFinishMemoryCache<object> cache { get; set; }
        public static TomlTable blame { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            MainAsync().GetAwaiter().GetResult();

            Log.CloseAndFlush();
        }

        static async Task MainAsync()
        {
            //Globals.cache = new WaitToFinishMemoryCache<object>;
            try
            {
                using (StreamReader reader = new StreamReader(File.OpenRead("config.toml")))
                {
                    Globals.config = TOML.Parse(reader);
                }
            }
            catch
            {
                Console.Write("Problem reading config file.  Exiting.\n");
                System.Environment.Exit(1);
            }
            Web.WebConsts.RootUrl = Globals.config["fio"]["host"];

            var discord = new DiscordClient(new DiscordConfiguration()
            {
                Token = Globals.config["discord"]["token"],
                TokenType = TokenType.Bot
            });

            var prefix = Globals.config["discord"]["prefix"].AsString.Value;
            var commands = discord.UseCommandsNext(new CommandsNextConfiguration()
            {
                StringPrefixes = new[] { prefix },
            });

            commands.RegisterCommands<commands.BuildingCommands>();
            commands.RegisterCommands<commands.ChatCommands>();
            commands.RegisterCommands<commands.MaterialCommands>();
            commands.RegisterCommands<commands.PlanetCommands>();

            await discord.ConnectAsync();
            await msgchan(discord);
            await Task.Delay(-1);
        }

        static async Task msgchan(DiscordClient discord)
        {
            if(Globals.config["discord"].HasKey("debug_channel"))
            {
                string chanid_str = Globals.config["discord"]["debug_channel"];
                ulong debug_chanid = Convert.ToUInt64(chanid_str);
                var channel = await discord.GetChannelAsync(debug_chanid);
                await discord.SendMessageAsync(channel, "I'm alive!");
            }
            // Kick off caching of the big (static) queries
            // Buildings, planets?, etc.
            Fido.commands.ChatCommands.ReloadBlame();
            await Planets.Find("Katoa");
            await Buildings.FindByTicker("rig");
            await Materials.FindByTicker("dw");
            await Recipes.FindByOutputTicker("h20");
            await Exchanges.GetExchangeList();
        }
    }
}
